package com.example.counter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private val stateName:String = "tv_counter_text"
    private var counter:Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
// Adding numbers
    fun addNumber(view: android.view.View){
        if(counter < Int.MAX_VALUE) {
            counter++
            tvCounterUpdate() // Some update
            // Some Feature add
            // Continuous fixing
        }
    }

    private fun tvCounterUpdate(){
        val tv = findViewById<TextView>(R.id.tv_counter)
        tv.text = counter.toString()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(stateName,counter)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        counter = savedInstanceState.getInt(stateName)
        tvCounterUpdate()
    }
}